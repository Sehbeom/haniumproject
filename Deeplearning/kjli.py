import tensorflow.keras
from PIL import Image, ImageOps
import numpy as np
import cv2

# Disable scientific notation for clarity
np.set_printoptions(suppress=True)

# Load the model
model = tensorflow.keras.models.load_model('keras_model.h5')

# Create the array of the right shape to feed into the keras model
# The 'length' or number of images you can put into the array is
# determined by the first position in the shape tuple, in this case 1.
data = np.ndarray(shape=(1, 224, 224, 3), dtype=np.float32)

# Replace this with the path to your image
#image = Image.open('test_photo.jpg')
image = cv2.VideoCapture(0)
image.set(cv2.CAP_PROP_FRAME_WIDTH, 224)
image.set(cv2.CAP_PROP_FRAME_HEIGHT, 224)
#resize the image to a 224x224 with the same strategy as in TM2:
#resizing the image to be at least 224x224 and then cropping from the center
size = (224.0, 224.0)
while True:
    #image = ImageOps.fit(image, size, Image.ANTIALIAS)
    ret, frame1 = image.read()
    frame = ImageOps.fit(frame1, size, Image.ANTIALIAS)
    #turn the image into a numpy array
    image_array = np.asarray(frame)

    # display the resized image
    #image.show()

    cv2.imshow("VideoFrame",frame)
    

    # Normalize the image
    normalized_image_array = (image_array.astype(np.float32) / 127.0) - 1

    # Load the image into the array
    data[0] = normalized_image_array

    # run the inference
    prediction = model.predict(data)
    print(prediction)
    if cv2.waitKey(1) > 0: break

image.release()
cv2.destroyAllWindow()
